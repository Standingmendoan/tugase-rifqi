public class MahasiswaSarjana extends Mahasiswa {
    private int jumlahPublikasi;

    public MahasiswaSarjana(String nama, String nim, int tahunMasuk) {
        super(nama, nim, tahunMasuk);
        jumlahSks = 0;
        ipk = 0;
        jumlahPublikasi = 0;
    }

    public void tambahPublikasi(int jumlah) {
        jumlahPublikasi += jumlah;
    }

    public int getJumlahPublikasi() {
        return jumlahPublikasi;
    }


    @Override
    public String getNama() {
        return super.getNama();
    }

    @Override
    public void setJumlahSks(int jumlahSks) {
        super.setJumlahSks(jumlahSks);
    }
}
