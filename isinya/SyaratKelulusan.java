import java.time.LocalDate;

public class SyaratKelulusan {

        // Deklarasi variabel data syarat kelulusan
        private int sksMin = 144;
        private double masaStudiMin = 3.5;
        private int publikasiMin = 1;
        private int sertifikatMin = 1;
        private int produkHkiMin = 1;

    public SyaratKelulusan(Mahasiswa mahasiswa) {

    }


    // Metode untuk memeriksa apakah mahasiswa telah memenuhi syarat kelulusan
        public boolean cekKelulusan(Mahasiswa mahasiswa) {
            boolean lulus = true;

            // Pengecekan jumlah SKS
            if (mahasiswa.getJumlahSks() < sksMin) {
                lulus = false;
            }

            // Pengecekan masa studi
            if (hitungMasaStudi(mahasiswa) < masaStudiMin) {
                lulus = false;
            }

            // Pengecekan publikasi (hanya untuk jenjang Sarjana)
            if (mahasiswa instanceof MahasiswaSarjana && ((MahasiswaSarjana) mahasiswa).getJumlahPublikasi() < publikasiMin) {
                lulus = false;
            }

            // Pengecekan sertifikat atau produk HKI (hanya untuk jenjang Diploma)
            if (mahasiswa instanceof MahasiswaDiploma) {
                MahasiswaDiploma mhsDiploma = (MahasiswaDiploma) mahasiswa;
                if ((mhsDiploma.getJumlahSertifikat() < sertifikatMin) && (mhsDiploma.getJumlahProdukHki() < produkHkiMin)) {
                    lulus = false;
                }
            }

            return lulus;
        }

        // Metode untuk menghitung masa studi mahasiswa dalam tahun
        private double hitungMasaStudi(Mahasiswa mahasiswa) {
            int tahunSekarang = LocalDate.now().getYear();
            return (tahunSekarang - mahasiswa.getTahunMasuk()) + ((double) mahasiswa.getNomerUrut() / 1000);
        }
    }


